import random, prettytable

def game():
    """
    Set up three doors, one randomly with a car behind and two with
    goats behind. Choose a door randomly, then the presenter takes away
    one of the goats. Return the outcome based on whether you stuck with
    your original choice or switched to the other remaining closed door.
    """
    # Neither stick or switch has won yet, so set them both to False
    stick = switch = False
    # Set all of the doors to goats (zeroes)
    doors = [ 0, 0, 0 ]
    # Randomly change one of the goats for a car (one)
    doors[random.randint(0, 2)] = 1
    # Randomly choose one of the doors out of the three
    choice = doors[random.randint(0, 2)]
    # If our choice was a car (a one)
    if choice == 1:
        # Then stick wins
        stick = True
    else:
        # Otherwise, because the presenter would take away the other
        # goat, switching would always win.
        switch = True
    return (stick, switch)


def game_times_100():
    """
    Play the game 100 times and count up how many times stick or switch wins
    """
    stick_wins = 0
    switch_wins = 0
    for i in range(0, 100):
        stick, switch = game()
        if stick:
            stick_wins += 1
        if switch:
            switch_wins += 1
    return (stick_wins, switch_wins)


if __name__ == "__main__":
    """
    Play the game 100 times ten thousand times. Count up how many times each
    player won out of 100 games, or whether it was a tie.
    Print the results out in a pretty table with totals.
    """
    # Set all counters to zero
    stick_wins = switch_wins = ties = 0
    # Set up the table headers
    t = prettytable.PrettyTable(
        ["Game Number", "Stick Wins", "Switch Wins", "Ties"])
    
    # Loop around this code ten thousand times
    for i in range(1, 10001):
        # Tell the user what progress we've made every 1000 loops
        if i % 1000 == 0:
            print "Played %d games out of 10000.." % i
        # Play the game 100 times
        stick, switch = game_times_100()
        # Set up the output for the table
        stick_str = "%d - WIN!" % stick if stick > switch else str(stick)
        switch_str = "%d - WIN!" % switch if switch > stick else str(switch)
        tie_str = "TIE!" if stick == switch else ""
        # Add the row to the table
        t.add_row(["%d x 100" % i, stick_str, switch_str, tie_str])
        # If stick won out of 100 games, add 1 to their win count
        if stick > switch:
            stick_wins += 1
        # If switch won out of 100 games, add 1 to their win count
        elif switch > stick:
            switch_wins += 1
        # If stick and switch both won 50 games, add 1 to the tie count
        else:
            ties += 1

    # Display the totals at the bottom of the table
    t.add_row(["Totals", stick_wins, switch_wins, ties])
    # Tell the user what we're upto
    print "Now compiling results table.."
    # Print out the table on the screen
    print t

